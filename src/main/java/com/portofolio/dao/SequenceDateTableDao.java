package com.portofolio.dao;

import java.util.List;

import javax.persistence.LockModeType;
import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.SequenceDateTable;
import com.portofolio.entity.vo.SequenceDateTableId;


@Repository("SequenceDateTableDao")
public interface SequenceDateTableDao extends CrudRepository<SequenceDateTable, SequenceDateTableId> {
	
//	@Lock(LockModeType.PESSIMISTIC_WRITE)
//	@QueryHints({@QueryHint(name = "javax.persistence.lock.timeout", value ="3000")})
//	@Query(queryByNamaTabelAndKodeProfile)
//	SequenceDateTable findByNamaTableAndKodeProfile(@Param("kdProfile") Integer kdProfile, @Param("namaTable") String namaTable);

	@Lock(LockModeType.PESSIMISTIC_WRITE)
	@QueryHints({@QueryHint(name = "javax.persistence.lock.timeout", value ="3000")})
	@Query(queryByNamaTabelAndKodeProfile)
	List<SequenceDateTable> findSequence(
			@Param("kdProfile") Integer kdProfile,
			@Param("namaTable")  String namaTable);
	
	String queryByNamaTabelAndKodeProfile = "select model " 
			+ " from SequenceDateTable model "
			+ " where model.id.kdProfile=:kdProfile  "
			+ " and model.id.namaTable=:namaTable ";
	
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	@QueryHints({@QueryHint(name = "javax.persistence.lock.timeout", value ="3000")})
	@Query(queryByNamaTabelAndKodeProfileAndDate)
	List<SequenceDateTable> findSequence(
			@Param("kdProfile") Integer kdProfile,
			@Param("namaTable")  String namaTable, 
			@Param("tanggalAwal")  Long tanggalAwal, 
			@Param("tanggalAkhir")  Long tanggalAkhir);
	
	String queryByNamaTabelAndKodeProfileAndDate = "select model " 
			+ " from SequenceDateTable model "
			+ " where model.id.kdProfile=:kdProfile  "
			+ " and model.id.namaTable=:namaTable "
			+ " and model.id.tanggalAwal=:tanggalAwal "
			+ " and model.id.tanggalAkhir=:tanggalAkhir ";
	
	
}
