package com.portofolio.dao.custom.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.portofolio.dao.custom.DaoCustom;
import com.portofolio.util.CommonUtil;

/*
 * modif by Syamsu
 */

@Repository("DaoCustom")
public class DaoCustomImpl implements DaoCustom {

	@PersistenceContext
	public EntityManager em;
	

	@Override
	public Integer getKodeByNegara(String entity, Integer kdNegara,String field) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(" select max(cast(model.id."+field+" as int)) from ");
		buffer.append(entity);
		buffer.append(" model where model.id.kdNegara=:kdNegara ");
		Query query = em.createQuery(buffer.toString());
		query.setParameter("kdNegara", kdNegara);

		if (CommonUtil.isNullOrEmpty(query.getSingleResult())) {
			return 1;
		}
		return (int) query.getSingleResult() + 1;
	}
	
	@Override
	public Integer getIdTerahirNoNegara(String entity,String field) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(" select max(cast(model."+field+" as int)) from ");
		buffer.append(entity);
		buffer.append(" model ");
		Query query = em.createQuery(buffer.toString());
		if (CommonUtil.isNullOrEmpty(query.getSingleResult())) {
			return 1;
		}
		return (int) query.getSingleResult() + 1;
	}

}
