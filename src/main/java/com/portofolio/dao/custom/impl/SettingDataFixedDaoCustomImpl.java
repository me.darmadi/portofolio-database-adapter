package com.portofolio.dao.custom.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.portofolio.DbTable;
import com.portofolio.dao.custom.SettingDataFixedDaoCustom;

/*
 * modif by Syamsu
 */

@Repository("SettingDataFixedDaoCustom")
public class SettingDataFixedDaoCustomImpl implements SettingDataFixedDaoCustom {

	@PersistenceContext
	public EntityManager em;
	

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getTabelName() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("select tbl.TABLE_NAME  as NamaTable from information_schema.tables tbl order by  tbl.TABLE_NAME asc ");
		
		Query query = em.createNativeQuery(buffer.toString());
		List<String> list = query.getResultList();
		
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getFieldTabel(String namaTabel) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("select COLUMN_NAME " + 
				"  from information_schema.columns " + 
				" where table_schema = 'MDM8' " + 
				"   and table_name = '"+namaTabel+"' ");
		buffer.append("order by COLUMN_NAME asc");
		
		Query query = em.createNativeQuery(buffer.toString()); 
		
		List<String> list = query.getResultList();
		
		return list;
	}

	@Override
	public boolean isNegara(String table) {
//			Query query = em.createNativeQuery("select col.name "
//					+ " FROM sys.columns col "
//					+ " INNER JOIN sys.tables tbl "
//					+ " ON col.object_id=tbl.object_id "
//					+ " WHERE tbl.Name=:table"
//					+ " and col.name = 'KdProfile'" );
//			
//			query.setParameter("table", table);
//			query.setMaxResults(1);
//			
//			return CommonUtil.isNullOrEmpty(query.getResultList());
		return DbTable.TBL_NEGARA_ONLY.contains(table);
	}
	
	@SuppressWarnings({ "rawtypes" })
	@Override
	public List queryThis(String nilai, Integer kode, String data, String info) {
		Query query = em.createNativeQuery(nilai);
		
		query.setMaxResults(1);
		
		query.setParameter("value", data);
		query.setParameter(info, kode);
		
		List list = query.getResultList();
		return list;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<String> getPK(String namaTabel) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("SELECT k.column_name FROM information_schema.table_constraints t JOIN information_schema.key_column_usage k USING(constraint_name,table_schema,table_name) WHERE t.constraint_type='PRIMARY KEY' AND t.table_schema='MDM8' AND t.table_name='"+namaTabel+"'");
		Query query = em.createNativeQuery(buffer.toString());
		List<String> list = query.getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getData(String namaTabel, String field1, String field2, Integer kdprofile, Integer kdNegara) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("SELECT concat(" + field1 + ",','," + field2 + ") from " + namaTabel);
		buffer.append(cekProfile(namaTabel, kdprofile, kdNegara));
		buffer.append(" order by " + field1 + " asc ");
		Query query = em.createNativeQuery(buffer.toString());
		List<String> list = query.getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getDataOneField(String namaTabel, String field1, Integer kdprofile, Integer kdNegara) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("SELECT CONVERT(varchar(500),  " + field1 + ") from " + namaTabel);
		buffer.append(cekProfile(namaTabel, kdprofile, kdNegara));
		buffer.append(" order by " + field1 + " asc ");
		Query query = em.createNativeQuery(buffer.toString());
		List<String> list = query.getResultList();
		return list;
	}

	@Override
	public String cekProfile(String namaTabel, Integer kdprofile, Integer kdNegara) {
		List<String> data = getPK(namaTabel);
		Boolean isProf = isNegara(namaTabel);
		for (String dt : data) {
			if (dt.equalsIgnoreCase("kdprofile") && !isProf) {
				return " where kdprofile=" + kdprofile + " and statusEnabled=1 ";
			}else if (dt.equalsIgnoreCase("kdnegara") && isProf) {
				return " where kdnegara=" + kdNegara + " and statusEnabled=1 ";
			}
		}
		return "";
	}
}
