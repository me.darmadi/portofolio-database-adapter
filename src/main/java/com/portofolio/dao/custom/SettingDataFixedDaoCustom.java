package com.portofolio.dao.custom;

import java.util.List;

/*
 * modif by Syamsu
 */

public interface SettingDataFixedDaoCustom {
	
	List<String> getTabelName();
	
	List<String> getFieldTabel(String namaTabel);	
	
	boolean isNegara(String table);
	
	@SuppressWarnings("rawtypes") List queryThis(String nilai, Integer kdProfile, String data, String info);
	
	List<String> getPK(String namaTabel);

	List<String> getDataOneField(String namaTabel, String field1, Integer kdprofile, Integer kdNegara);

	String cekProfile(String namaTabel, Integer kdprofile, Integer kdNegara);

	List<String> getData(String namaTabel, String field1, String field2, Integer kdprofile, Integer kdNegara);

}
