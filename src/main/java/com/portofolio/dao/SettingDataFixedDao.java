package com.portofolio.dao;

import java.util.Map;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.SettingDataFixed;
import com.portofolio.entity.vo.SettingDataFixedId;

/*
 * modifikasi Syamsu punya Andri dan pak Luqman
 */
@Repository("SettingdatafixedDao")
public interface SettingDataFixedDao extends CrudRepository<SettingDataFixed, SettingDataFixedId> {
	
	@Cacheable(cacheNames = "SettingDataFixedDaoFindOneByIdNamaFieldAndIdKdProfile")
    SettingDataFixed findOneByIdNamaFieldAndIdKdProfile(String namaField,Integer kdProfile);

	@Cacheable(cacheNames = "SettingDataFixedDaoFindOneByNamaFieldAndIdKdProfile")
	@Query(queryFindByNamaField)
	SettingDataFixed findOneByNamaFieldAndIdKdProfile(@Param("namaField") String namaField, @Param("kdProfile") Integer kdProfile);

	@Cacheable(cacheNames = "SettingDataFixedDao1")
	@Query(queryFindByNamaField)
	SettingDataFixed findByNamaField(@Param("namaField") String namaField, @Param("kdProfile") Integer kdProfile);
	
	   @Query(QListAll + " and model.id.namaField like :namaField ")
	    @Cacheable("SettingDataFixedDaoFindByNamaField")
	    Map<String, Object> findByNamaField(@Param("kdProfile") Integer kdProfile, @Param("namaField") String namaField);
	
	String queryFindByNamaField = "select model "
			+ " from SettingDataFixed model  "
			+ " where model.id.kdProfile=:kdProfile and model.statusEnabled=true "
			+ " and model.id.namaField=:namaField ";

	String QListAll ="select new map(model.id as namaField "
            + ", model.typeField as typeField " 
            + ", model.nilaiField as nilaiField " 
            + ", model.tabelRelasi as tabelRelasi " 
            + ", model.fieldKeyTabelRelasi as fieldKeyTabelRelasi " 
            + ", model.fieldReportDisplayTabelRelasi as fieldReportDisplayTabelRelasi " 
            + ", model.keteranganFungsi as keteranganFungsi " 
            + ", model.statusEnabled as statusEnabled " 
            + ", model.noRec as noRec " 
            + ", model.version as version )" 
            + " from SettingDataFixed model "
            + "left join model.id id "
            + "where id.kdProfile=:kdProfile  "; 
}
