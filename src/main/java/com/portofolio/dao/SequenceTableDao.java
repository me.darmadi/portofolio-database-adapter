package com.portofolio.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.SequenceTable;
import com.portofolio.entity.vo.SequenceTableId;
/*
 * modifikasi Syamsu punya mas Adik
 */

@Repository("SequenceTableDao")
public interface SequenceTableDao extends CrudRepository<SequenceTable, SequenceTableId> {
	
	String queryByNamaTabelAndKodeProfile = "select model " 
			+ " from SequenceTable model "
			+ " where model.id.kdProfile=:kdProfile  "
			+ " and model.id.namaTable=:namaTable ";
	
	@Query(queryByNamaTabelAndKodeProfile)
	List<SequenceTable> findListByNamaTableAndKodeProfile(@Param("namaTable")  String namaTable, @Param("kdProfile") Integer kdProfile);
	
	
	
}
