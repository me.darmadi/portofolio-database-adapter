package com.portofolio.base;

import java.util.Map;

public interface BaseDao<T,V> {

	T save(T obj);
	
	T update(T obj);

	T findByKode(Integer kode, Integer kdProfile);

	T findByKode(String kode, Integer kdProfile);
	
	T findbyId(Map<String, Object> value);
}
