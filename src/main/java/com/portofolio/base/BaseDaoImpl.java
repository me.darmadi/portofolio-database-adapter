package com.portofolio.base;

import java.lang.reflect.Field;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

@Repository
public abstract class BaseDaoImpl<T,V> implements BaseDao<T,V> {
	
	@PersistenceContext
	public EntityManager em;

	protected abstract Class<T> getDomainClass();
	
	protected abstract Class<V> getIdClass();
	

	@Override
	public T save(T obj) {
		em.persist(obj);
		return obj;
    }
	
	@Override
	public T update(T obj) {
		obj = em.merge(obj);
		return obj;
    }
	
	private String getQuery() {
		return "from " 
				+ getDomainClass().getSimpleName() 
				+ " domain "
				+ " where domain.statusEnabled=true "
				+ " and domain.id.kdProfile=:kdProfile "
				+ " and domain.id.kode =:kode ";
	}

	@SuppressWarnings("unchecked")
	@Override
	public T findByKode(Integer kode, Integer kdProfile) {
		Query query = em.createQuery(getQuery());
		
		query.setParameter("kode", kode);
		query.setParameter("kdProfile", kdProfile);
		return (T) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public T findByKode(String kode, Integer kdProfile) {
		Query query = em.createQuery(getQuery());
		
		query.setParameter("kode", kode);
		query.setParameter("kdProfile", kdProfile);
		return (T) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public T findbyId(Map<String, Object> value) {
		
		Field[] fs = getIdClass().getFields();
		
		StringBuilder sb = new StringBuilder();
		
		
		
		for (Field f:fs) {
			String name = f.getName();
			sb.append(" and domain.id.")
			.append(name)
			.append("=:")
			.append(name);
		}
		
		Query query = em.createQuery("from " 
				+ getDomainClass().getSimpleName() 
				+ " domain "
				+ " where domain.statusEnabled=true "
				+ sb.toString());
		
		for (Field f:fs) {
			String name = f.getName();
			query.setParameter(name, value.get(name));
		}

		return (T) query.getSingleResult();
	}
	
}
