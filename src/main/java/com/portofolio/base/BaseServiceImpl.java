package com.portofolio.base;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.constant.BaseConstant;
import com.portofolio.dao.SequenceDateRuanganTableDao;
import com.portofolio.dao.SequenceDateTableDao;
import com.portofolio.dao.SequenceTableDao;
import com.portofolio.dao.custom.DaoCustom;
import com.portofolio.dto.SessionDto;
import com.portofolio.entity.SequenceDateRuanganTable;
import com.portofolio.entity.SequenceDateTable;
import com.portofolio.entity.SequenceTable;
import com.portofolio.entity.SettingDataFixed;
import com.portofolio.entity.vo.SequenceDateRuanganTableId;
import com.portofolio.entity.vo.SequenceDateTableId;
import com.portofolio.entity.vo.SequenceTableId;
import com.portofolio.exception.CustomException;
import com.portofolio.exception.InfoException;
import com.portofolio.exception.NotFoundException;
import com.portofolio.service.LoginUserService;
import com.portofolio.service.SessionService;
import com.portofolio.service.SettingDataFixedService;
import com.portofolio.util.CommonUtil;
import com.portofolio.util.DateUtil;


 

@Service
public class BaseServiceImpl implements BaseService {

	@Autowired SettingDataFixedService settingDataFixedService;
	
	@Autowired SequenceTableDao sequenceTableDao;
	
	@Autowired SequenceDateTableDao sequenceDateTableDao;
	
	@Autowired SequenceDateRuanganTableDao sequenceDateRuanganTableDao;
	
	@Autowired LoginUserService loginUserService;

	@Autowired DaoCustom daoCustom;
	
	@Autowired @Qualifier("SessionService") SessionService sessionService;

	protected Map<String,Object> result=new HashMap<String,Object>();
	
	protected List<String> errors = new ArrayList<String>();
	
	protected static final boolean CONSTANT_STATUS_ENABLED_TRUE=true;
	
	protected static final boolean CONSTANT_STATUS_ENABLED_FALSE=false;
	
	
	public synchronized Map<String,Object> resultSuccessful(){
		result.put(BaseConstant.MESSAGE, BaseConstant.HttpHeaderInfo.REQUEST_SUCCESSFUL);
		result.put(BaseConstant.STATUS, HttpStatus.OK.name());
		result.put(BaseConstant.STATUS_CODE,  HttpStatus.OK.toString());
		result.put(BaseConstant.RESULT, HttpStatus.OK);
		return result;
		
	}
	
	
	public Map<String,Object> resultDeleted(){
		result.put(BaseConstant.MESSAGE, BaseConstant.HttpHeaderInfo.DATA_DELETED);
		result.put(BaseConstant.STATUS, HttpStatus.OK.name());
		result.put(BaseConstant.STATUS_CODE,  HttpStatus.OK.toString());
		result.put(BaseConstant.RESULT, HttpStatus.OK);
		return result;
		
	}
	
	public Map<String, Object> resultUpdated() {
		result.put(BaseConstant.MESSAGE, BaseConstant.HttpHeaderInfo.DATA_UPDATED);
		result.put(BaseConstant.STATUS, HttpStatus.OK.name());
		result.put(BaseConstant.STATUS_CODE, HttpStatus.OK.toString());
		result.put(BaseConstant.RESULT, HttpStatus.OK);
		return result;}
	
	public Map<String,Object> resultCreated(){
		result.put(BaseConstant.MESSAGE, BaseConstant.HttpHeaderInfo.DATA_CREATED);
		result.put(BaseConstant.STATUS, HttpStatus.CREATED.name());
		result.put(BaseConstant.STATUS_CODE,  HttpStatus.CREATED.toString());
		result.put(BaseConstant.RESULT, HttpStatus.CREATED);
		return result;
	}
	
	public Map<String,Object> resultError(Exception e) {
		e.printStackTrace();
		throw new CustomException(e.toString());
//		List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
//
//		Map<String, Object> rm = new java.util.HashMap<String, Object>();
//
//		if (e instanceof ConstraintViolationException) {
//			rm.put("error", "Constrain Violation Exception");
//		} else if (e instanceof ObjectOptimisticLockingFailureException) {
//			rm.put("error", "Data was update by other transaction");
//		} else if (e instanceof InfoException){
//			String message=e.toString(); 
//			message=message.replaceAll("com.portofolio.exception.InfoException: ","");//replaces all occurrences of "is" to "was"  
//			rm.put("error", message);
//		} else if (e instanceof Exception){
//			String message=e.toString(); 
//			message=message.replaceAll("java.lang.Exception: ","");//replaces all occurrences of "is" to "was"  
//			rm.put("error", message);
//		}else {
//			rm.put("error", e.toString());
//		}
//		data.add(rm);
//
//		Map<String,Object> mapHeaderMessage=new HashMap<>();
//		result.put(BaseConstant.MESSAGE, BaseConstant.HttpHeaderInfo.REQUEST_UNSUCCESSFUL);
//		result.put(BaseConstant.STATUS, HttpStatus.INTERNAL_SERVER_ERROR.name());
//		result.put(BaseConstant.STATUS_CODE,  HttpStatus.INTERNAL_SERVER_ERROR.toString());
//		result.put("errors", data);
//		result.put(BaseConstant.RESULT, HttpStatus.INTERNAL_SERVER_ERROR);
//		result.put("mapHeaderMessage",  mapHeaderMessage);
	
	}
	
	public Map<String,Object> resultNotFound(){
		throw new NotFoundException("data to be deleted or changed is not found!");
		
	}
	
	@Override
	public String generateUuid32() {
		UUID data = UUID.randomUUID(); 
		return data.toString();
	}
	
	@Override
	public String generateUuid16() {
		String suuid = UUID.randomUUID().toString();
		String partial_id = suuid.substring(14,18) +"-"+ suuid.substring(9, 13) +"-"+ suuid.substring(0, 4) +"-"+ suuid.substring(19, 23) ;
		return partial_id;
	}
	
	@Override
	public Integer getKdProfile() {
		return sessionService.getSession().getKdProfile();
	}
	
	@Override
	public Integer getKdNegara() {
		return sessionService.getSession().getKdNegaraProfile();
	}

	@Override
	@Transactional(readOnly = false)
	public Integer getIdTerahir(String entityClass) {
		
	List<SequenceTable> lsequenceTabel = sequenceTableDao.findListByNamaTableAndKodeProfile(entityClass,getKdProfile());

		if (lsequenceTabel.isEmpty()) {
			SequenceTable sequenceTable = new SequenceTable();
			SequenceTableId id = new SequenceTableId();
			id.setKdProfile(getKdProfile());
			id.setNamaTable(entityClass);
			// id.setKdSequenceTable(generateUuid());
			sequenceTable.setId(id);
			sequenceTable.setIdTerakhir(1);
			sequenceTable.setVersion(1);
			// sequenceTable.setNamaTable(sequenceTabelService.getTableName(entityClass));
			sequenceTable.setNoRec(generateUuid32());
			sequenceTableDao.save(sequenceTable);
			return 1;
		} else {
			SequenceTable sequenceTable = lsequenceTabel.get(0);
			Integer idTerakhir = sequenceTable.getIdTerakhir();
			if (CommonUtil.isNotNullOrEmpty(idTerakhir)) {
				idTerakhir += 1;
				sequenceTable.setIdTerakhir(idTerakhir);
				sequenceTableDao.save(sequenceTable);
				return idTerakhir;
			} else {
				sequenceTable.setIdTerakhir(1);
				sequenceTableDao.save(sequenceTable);
				return 1;
			}
		}
	}
	
	
	@Override
	@Transactional(readOnly = false)
	public Integer getIdTerahirResetByDay(String entityClass,Long date) {
	Long tanggalAwal=DateUtil.periodeAwalHari(date);
	Long tanggalAkhir=DateUtil.periodeAkhirHari(date);
	List<SequenceDateTable> lsequenceTabel = sequenceDateTableDao.findSequence(getKdProfile(), entityClass, tanggalAwal, tanggalAkhir);

	if(lsequenceTabel.size()>1) {
		throw new InfoException("Kesalahan Komunikasi server,, Silahkan Hubungi Aministrator");
	}
		if (lsequenceTabel.isEmpty()) {
			SequenceDateTable sequenceTable = new SequenceDateTable();
			SequenceDateTableId id = new SequenceDateTableId();
			id.setKdProfile(getKdProfile());
			id.setNamaTable(entityClass);
			id.setTanggalAwal(tanggalAwal);
			id.setTanggalAkhir(tanggalAkhir);
			// id.setKdSequenceTable(generateUuid());
			sequenceTable.setId(id);
			sequenceTable.setIdTerakhir(1);
			sequenceTable.setVersion(1);
			// sequenceTable.setNamaTable(sequenceTabelService.getTableName(entityClass));
			sequenceTable.setNoRec(generateUuid32());
			sequenceDateTableDao.save(sequenceTable);
			return 1;
		} else {
			SequenceDateTable sequenceTable = lsequenceTabel.get(0);
			Integer idTerakhir = sequenceTable.getIdTerakhir();
			if (CommonUtil.isNotNullOrEmpty(idTerakhir)) {
				idTerakhir += 1;
				sequenceTable.setIdTerakhir(idTerakhir);
				sequenceDateTableDao.save(sequenceTable);
				return idTerakhir;
			} else {
				sequenceTable.setIdTerakhir(1);
				sequenceDateTableDao.save(sequenceTable);
				return 1;
			}
		}
	}
	@Override
	public  String getIdTerahir(String entityClass, int pad) {
		Integer idTerakhir = getIdTerahir(entityClass);
		return String.format("%0" + pad + "d", idTerakhir);
		// return StringUtils.leftPad(String.valueOf(idTerakhir), pad, fill);
	}
	
	@Override
	public  String getIdTerahirResetByDay(String entityClass,Long date,int pad) {
		Integer idTerakhir = getIdTerahirResetByDay(entityClass, date);
		return String.format("%0" + pad + "d", idTerakhir);
		// return StringUtils.leftPad(String.valueOf(idTerakhir), pad, fill);
	}
	
	@Override
	public  String getIdTerahirResetByDayRuangan(String entityClass,Long date,String kdRuangan,int pad) {
		Integer idTerakhir = getIdTerahirResetByDayKdRuangan(entityClass, date, kdRuangan);
		return String.format("%0" + pad + "d", idTerakhir);
		// return StringUtils.leftPad(String.valueOf(idTerakhir), pad, fill);
	}


	@Override
	public SessionDto getSession() {
		return sessionService.getSession();
	}


	@Override
	public <T> Integer getIdTerahirByNegara(Class<T> class1, String field) {
		// TODO Auto-generated method stub
		Integer kdNegara=loginUserService.getSession().getKdNegaraProfile();
		return daoCustom.getKodeByNegara(class1.getName(), kdNegara,field);
	}
	
	
	@Override
	public <T> Integer getIdTerahirNoNegara(Class<T> class1,String field) {
		// TODO Auto-generated method stub
		return daoCustom.getIdTerahirNoNegara(class1.getName(),field);
	}
	
	@Override
	public SettingDataFixed getSettingDataFixed(String namaField) {
		return getSettingDataFixed(getKdProfile(), namaField);
	}
	
	@Override
	public SettingDataFixed getSettingDataFixed(Integer kdProfile, String namaField) {
		if (CommonUtil.isNullOrEmpty(namaField)) {
			throw new InfoException("Nama SettingDataFixed tidak boleh kosong ");
		}
		SettingDataFixed settingDataFixed = settingDataFixedService.findByNamaField(kdProfile, namaField.trim());
		
		checkSettingDataFixed(kdProfile, namaField, settingDataFixed);
		
		return settingDataFixed;
	}

	/////////////////////////////
	///// SETTING DATAFIXED /////	
	private void checkSettingDataFixed(Integer kdProfile, String namaField, SettingDataFixed settingDataFixed) {
		if (CommonUtil.isNullOrEmpty(settingDataFixed)) {
			throw new InfoException("SettingDataFixed " + namaField + " tidak ditemukan, harap disetting terlebih dahulu.");
		}
		
//
//     DIBUKA LAGI KALAU SUDAH BEBAS ERROR 
//	
//		String table = settingDataFixed.getTabelRelasi();
//		String field = settingDataFixed.getFieldKeyTabelRelasi();
//		
//		if (CommonUtil.isNotNullOrEmpty(table)) {
//			if (CommonUtil.isNullOrEmpty(field)) {
//				throw new InfoException("SettingDataFixed " + namaField + " tidak bisa dicek karena 'Field Key Table Relasi' tidak diisi, harap perbaiki sebelum melanjutkan.");
//			} else {
//				try {
//					if (settingDataFixedDaoCustom.isNegara(table)) {
//						findByKdNegara(settingDataFixed,  getKdNegaraProfile(), table, field, namaField);
//					} else {
//						findByKdProfile(settingDataFixed, kdProfile, table, field, namaField);
//					}
//				}catch(Exception e) {
//					StringUtil.printStackTrace(e);
//					throw new InfoException("SettingDataFixed " + namaField + ", 'Field Key Table Relasi' tidak tepat nilainya pastikan ini bukan tipe database tapi tipe teks.");					
//				}
//			}			
//		}
//		
		if ("Integer".equalsIgnoreCase(settingDataFixed.getTypeField())) {
			try {
				Integer.parseInt(settingDataFixed.getNilaiField());
			} catch (Exception e) {
				throw new InfoException("SettingDataFixed " + namaField + " bukan type data Integer, pastikan sesuai panduan.");
			}
		} else if ("Float".equalsIgnoreCase(settingDataFixed.getTypeField())
				|| "Real".equalsIgnoreCase(settingDataFixed.getTypeField())) {
			try {
				Float.parseFloat(settingDataFixed.getNilaiField());
			} catch (Exception e) {
				throw new InfoException("SettingDataFixed " + namaField + " bukan type data Real / Float, pastikan sesuai panduan.");
			}
		} else if ("Double".equalsIgnoreCase(settingDataFixed.getTypeField())
				|| "Decimal".equalsIgnoreCase(settingDataFixed.getTypeField())
				|| "Numeric".equalsIgnoreCase(settingDataFixed.getTypeField())) {
			try {
				Double.parseDouble(settingDataFixed.getNilaiField());
			} catch (Exception e) {
				throw new InfoException("SettingDataFixed " + namaField	+ " bukan type data Double / Decimal / Numeric, pastikan sesuai panduan.");
			}
		} else if ("BigDecimal".equalsIgnoreCase(settingDataFixed.getTypeField())) {
			try {
				new BigDecimal(settingDataFixed.getNilaiField());
			} catch (Exception e) {
				throw new InfoException("SettingDataFixed " + namaField + " bukan type data BigDecimal, pastikan sesuai panduan.");
			}
		}
	}
	
	public String generateUniqueKey() {
		int leftLimit = 48; // numeral '0'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = 5;
	    Random random = new Random();
	 
	    String generatedString = random.ints(leftLimit, rightLimit + 1)
	      .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
	      .limit(targetStringLength)
	      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
	      .toString();
	 
	   return generatedString;
	}

	@Override
	@Transactional(readOnly = false)
	public Integer getIdTerahirResetByDayKdRuangan(String entityClass,Long date,String kdRuangan) {
	Long tanggalAwal=DateUtil.periodeAwalHari(date);
	Long tanggalAkhir=DateUtil.periodeAkhirHari(date);
	List<SequenceDateRuanganTable> lsequenceTabel = sequenceDateRuanganTableDao.findSequence(getKdProfile(), entityClass, tanggalAwal, tanggalAkhir,kdRuangan);

	if(lsequenceTabel.size()>1) {
		throw new InfoException("Kesalahan Komunikasi server,, Silahkan Hubungi Aministrator");
	}
		if (lsequenceTabel.isEmpty()) {
			SequenceDateRuanganTable sequenceTable = new SequenceDateRuanganTable();
			SequenceDateRuanganTableId id = new SequenceDateRuanganTableId();
			id.setKdProfile(getKdProfile());
			id.setNamaTable(entityClass);
			id.setTanggalAwal(tanggalAwal);
			id.setTanggalAkhir(tanggalAkhir);
			id.setKdRuangan(kdRuangan);
			// id.setKdSequenceTable(generateUuid());
			sequenceTable.setId(id);
			sequenceTable.setIdTerakhir(1);
			sequenceTable.setVersion(1);
			sequenceTable.setNoRec(generateUuid32());
			sequenceDateRuanganTableDao.save(sequenceTable);
			return 1;
		} else {
			SequenceDateRuanganTable sequenceTable = lsequenceTabel.get(0);
			Integer idTerakhir = sequenceTable.getIdTerakhir();
			if (CommonUtil.isNotNullOrEmpty(idTerakhir)) {
				idTerakhir += 1;
				sequenceTable.setIdTerakhir(idTerakhir);
				sequenceDateRuanganTableDao.save(sequenceTable);
				return idTerakhir;
			} else {
				sequenceTable.setIdTerakhir(1);
				sequenceDateRuanganTableDao.save(sequenceTable);
				return 1;
			}
		}
	}
	


}
