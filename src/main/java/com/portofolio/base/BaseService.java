package com.portofolio.base;

import com.portofolio.dto.SessionDto;
import com.portofolio.entity.SettingDataFixed;

public interface BaseService {

	String generateUuid32();

	Integer getKdProfile();

	Integer getIdTerahir(String entityClass);

	SessionDto getSession();

	String getIdTerahir(String entityClass, int pad);
	

	Integer getKdNegara();

	<T>Integer getIdTerahirByNegara(Class<T> class1, String field);

	<T>Integer getIdTerahirNoNegara(Class<T> class1, String field);

	SettingDataFixed getSettingDataFixed(String namaField);

	SettingDataFixed getSettingDataFixed(Integer kdProfile, String namaField);

	Integer getIdTerahirResetByDay(String entityClass, Long date);

	String getIdTerahirResetByDay(String entityClass, Long date, int pad);

	String generateUuid16();

	Integer getIdTerahirResetByDayKdRuangan(String entityClass, Long date, String kdRuangan);

	String getIdTerahirResetByDayRuangan(String entityClass, Long date, String kdRuangan, int pad);
	
	

	
}
