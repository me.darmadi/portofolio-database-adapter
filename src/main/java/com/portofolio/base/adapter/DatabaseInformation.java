package com.portofolio.base.adapter;

import lombok.Getter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
/**
*
* @author 
*/
@Component
@Configuration
@Getter
public class DatabaseInformation {
	
	@Value("${spring.datasource.driverClassName}")
	String spring_datasource_driverClassName;
	
	@Value("${spring.datasource.jdbcUrl}")
	String spring_datasource_jdbcUrl;
	
	@Value("${spring.datasource.poolName}")
	String spring_datasource_poolName;
	
	@Value("${spring.datasource.maximumPoolSize}")
	String spring_datasource_maximumPoolSize;
	
	@Value("${spring.datasource.minimumIdle}")
	String spring_datasource_minimumIdle;
	
	@Value("${spring.datasource.idleTimeout}")
	String spring_datasource_idleTimeout;
	
	@Value("${spring.jpa.show-sql}")
	boolean spring_jpa_showSql;
	
	@Value("${spring.jpa.properties.hibernate.hbm2ddl.auto}")
	String spring_jpa_properties_hibernate_ddlAuto;
	
	@Value("${spring.jpa.properties.hibernate.dialect}")
	String spring_jpa_properties_hibernate_dialect;
	
	@Value("${spring.jpa.hibernate.naming.physical-strategy}")
	String spring_jpa_hibernate_naming_physicalStrategy;
	
	@Value("${spring.jta.enabled}")
	String spring_jta_enabled;
}
