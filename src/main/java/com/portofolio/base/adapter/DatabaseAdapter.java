package com.portofolio.base.adapter;

/**
*
* @author 
*/
public class DatabaseAdapter {

	public static final class Constant {

		public static final String AWAL_KURUNG = "(";
		public static final String AKHIR_KURUNG = ")";

		public static final String AWAL_KURUNG_KURAWAL = "{";
		public static final String AKHIR_KURUNG_KURAWAL = "}";

		public static final String AWAL_KURUNG_SIKU = "[";
		public static final String AKHIR_KURUNG_SIKU = "]";

		public static final String INTEGER = "INTEGER";
		public static final String BIGINT = "BIGINT";
		public static final String BYTE = "BYTE";
		public static final String VARCHAR = "VARCHAR";
		public static final String CHAR = "CHAR";
		public static final String REAL = "REAL";
		public static final String DATE = "DATE";
		public static final String TIME = "TIME";
		public static final String DATETIME = "DATETIME";
		public static final String SMALLDATETIME = "SMALLDATETIME";
		public static final String IMAGE = "IMAGE";
		public static final String BLOB = "BLOB";
		public static final String FLOAT = "FLOAT";
		public static final String BIT = "BIT";
		public static final String TIMESTAMP = "TIMESTAMP";
		public static final String DOUBLE = "DOUBLE";
		public static final String BIGDECIMAL = "DECIMAL";
		public static final String DECIMAL = "DECIMAL";
		public static final String TEXT = "TEXT";
		
	}

}
