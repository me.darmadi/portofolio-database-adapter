package com.portofolio;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import javax.sql.DataSource;
import com.zaxxer.hikari.HikariDataSource;
/**
*
* @author 
*/

@Configuration
@EnableJpaRepositories("com.portofolio.dao")
@EnableTransactionManagement
@EntityScan(basePackages = "com.portofolio.entity")
public class DbConfig {

	final Logger logger = LoggerFactory.getLogger(DbConfig.class);

	static DataSource ds;

	@Bean
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource dataSource() {
		if (ds == null) {
			ds = DataSourceBuilder.create().build();
			Runtime.getRuntime().addShutdownHook(new Thread() {
				@Override
				public void run() {
					if (ds instanceof HikariDataSource) {
						((HikariDataSource) ds).close();
					}
				}
			});
		}

		return ds;
	}
}

//@Bean
//public SessionFactory sessionFactory(HibernateEntityManagerFactory hemf) {
//	return hemf.getSessionFactory();
//}