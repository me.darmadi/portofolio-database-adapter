package com.portofolio;

import java.util.List;

import com.google.common.collect.Lists;

public final class DbTable {
	
	public static final List<String> TBL_NEGARA_ONLY = Lists.newArrayList(
			"Agama_M",
			"Bahasa_M",
			"Bulan_M",
			"DesaKelurahan_M",
			"GeneralDetailJenisProduk_M",
			"GeneralJenisProduk_M",
			"GeneralKelompokProduk_M",
			"GeneralProduk_M",
			"GeneralProdukInteraksi_M",
			"GolonganDarah_M",
			"Hari_M",
			"JenisKelamin_M",
			"Kecamatan_M",
			"KotaKabupaten_M",
			"MataUang_M",
			"MerkProduk_M",
			"Negara_M",
			"ProdusenProduk_M",
			"Propinsi_M",
			"Suku_M",
			"Transmisi_M",
			"TypeInteraksiProduk_M",
			"Zodiak_M",
			"ZodiakUnsur_M",
			"ZodiakUnsurSifatRange_M"
	);
}
