package com.portofolio.service;

import com.portofolio.entity.SettingDataFixed;

public interface SettingDataFixedService {
	
	SettingDataFixed findByNamaField(Integer kdProfile, String namaField);

}
