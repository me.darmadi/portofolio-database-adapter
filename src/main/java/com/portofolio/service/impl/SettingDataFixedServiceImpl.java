package com.portofolio.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.SettingDataFixedDao;
import com.portofolio.entity.SettingDataFixed;
import com.portofolio.service.SettingDataFixedService;

@Service("SettingDataFixedService")
public class SettingDataFixedServiceImpl extends BaseServiceImpl implements SettingDataFixedService {
	// @Autowired
	// private SettingDataFixedDaoCustom settingDataFixedDaoCustom;

	@Autowired
	private SettingDataFixedDao settingDataFixedDao;

	@Override
	public SettingDataFixed findByNamaField(Integer kdProfile, String namaField) {
		SettingDataFixed settingDataFixed = settingDataFixedDao.findByNamaField(namaField, kdProfile);
		return settingDataFixed;
	}
}
