package com.portofolio;

import java.util.List;
import java.util.Map;

import com.portofolio.util.CommonUtil;


public final class DbStrukturNomor {
	// format kode
	public static final String _yy = "yy";
	public static final String _mm = "mm";
	public static final String _dd = "dd";

	public static final String _yy_Romawi = "yy Romawi";
	public static final String _mm_Romawi = "mm Romawi";
	public static final String _dd_Romawi = "dd Romawi";

	public static final String _No_Urut = "No Urut";
	public static final String _Separator = "Separator";
	public static final String _String = "String";
	
	public static final String _Departemen = "Departemen"; 

	// status reset
	public static final String _ResetMonth = "M";
	public static final String _ResetYear = "Y";
	public static final String _ResetDay = "D";
	public static final String _NoReset = "N";
	public static final String _Antrian = "A";

	
	// status reset
		public static final String _NoUrutTabel = "NoUrutTabel";
		public static final String _NoUrutRuangan = "NoUrutRuangan";
		public static final String _NoUrutUser = "NoUrutUser";

	// status reset
	public static List<Map<String, Object>> statusResetList() {
		List<Map<String, Object>> myList = CommonUtil.createList();
		Map<String, Object> myMap = CommonUtil.createMap(); 
		myMap.put("value", _ResetMonth);
		myMap.put("nama", "mm");
		myList.add(myMap);
		myMap = CommonUtil.createMap();
		myMap.put("value", _ResetYear);
		myMap.put("nama", "yy");
		myList.add(myMap);
		myMap = CommonUtil.createMap();
		myMap.put("value", _ResetDay);
		myMap.put("nama", "dd");
		myList.add(myMap);
		myMap = CommonUtil.createMap();
		myMap.put("value", _NoReset);
		myMap.put("nama", "NoReset");
		myList.add(myMap);
		myMap = CommonUtil.createMap();
		myMap.put("value", _Antrian);
		myMap.put("nama", "Antrian");
		myList.add(myMap);
		return myList;
	}

	// format kode
	public static List<Map<String, Object>> formatKode() {
		List<Map<String, Object>> myList = CommonUtil.createList();
		Map<String, Object> myMap = CommonUtil.createMap();
		myMap.put("value", _yy);
		myMap.put("nama", "yy");
		myList.add(myMap);
		myMap = CommonUtil.createMap();
		myMap.put("value", _mm);
		myMap.put("nama", "mm");
		myList.add(myMap);
		myMap = CommonUtil.createMap();
		myMap.put("value", _dd);
		myMap.put("nama", "dd");
		myList.add(myMap);
		myMap = CommonUtil.createMap();
		myMap.put("value", _yy_Romawi);
		myMap.put("nama", "yy Romawi");
		myList.add(myMap);
		myMap = CommonUtil.createMap();
		myMap.put("value", _mm_Romawi);
		myMap.put("nama", "mm Romawi");
		myList.add(myMap);
		myMap = CommonUtil.createMap();
		myMap.put("value", _dd_Romawi);
		myMap.put("nama", "dd Romawi");
		myList.add(myMap);
		myMap = CommonUtil.createMap();
		myMap.put("value", _No_Urut);
		myMap.put("nama", "No Urut");
		myList.add(myMap);
		myMap = CommonUtil.createMap();
		myMap.put("value", _Separator);
		myMap.put("nama", "Separator");
		myList.add(myMap);
		myMap = CommonUtil.createMap();
		myMap.put("value", _String);
		myMap.put("nama", "String");
		myList.add(myMap);
		myMap = CommonUtil.createMap();
		myMap.put("value", _Departemen);
		myMap.put("nama", _Departemen);
		myList.add(myMap);
		return myList;
	}

}
