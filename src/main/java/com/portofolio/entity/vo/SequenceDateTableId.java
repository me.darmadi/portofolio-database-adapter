package com.portofolio.entity.vo;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class SequenceDateTableId implements java.io.Serializable {

	private static final String cDefKdProfile = INTEGER;
	@Column(name = "KdProfile", nullable = false, columnDefinition = cDefKdProfile)
	@NotNull(message = "KdProfile Harus Diisi")
	private Integer kdProfile;

	@Column(name = "namaTable")
	@NotNull(message = "nama Table harus di isi")
	private String namaTable;
	
	@Column(name = "TanggalAwal")
	private Long tanggalAwal;

	@Column(name = "TanggalAkhir")
	private Long tanggalAkhir;
}
