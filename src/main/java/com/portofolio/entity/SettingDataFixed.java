package com.portofolio.entity;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.VARCHAR;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.vo.SettingDataFixedId;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Table(name = "SettingDataFixed_M")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class SettingDataFixed {
	
    @EmbeddedId
    private SettingDataFixedId id ;
    
    private static final String cDefTypeField = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "TypeField", columnDefinition = cDefTypeField)
    @NotNull(message = "settingdatafixed.typefield.notnull")
    private String typeField;
    
    private static final String cDefNilaiField = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "NilaiField", columnDefinition = cDefNilaiField)
    @NotNull(message = "settingdatafixed.nilaifield.notnull")
    private String nilaiField;
    
    private static final String cDefTabelRelasi = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "TabelRelasi", columnDefinition = cDefTabelRelasi)
    private String tabelRelasi;
    
    private static final String cDefFieldKeyTabelRelasi = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "FieldKeyTabelRelasi", columnDefinition = cDefFieldKeyTabelRelasi)
    private String fieldKeyTabelRelasi;
    
    private static final String cDefFieldReportDisplayTabelRelasi = VARCHAR + AWAL_KURUNG + 50 + AKHIR_KURUNG ;
    @Column(name = "FieldReportDisplayTabelRelasi", columnDefinition = cDefFieldReportDisplayTabelRelasi)
    private String fieldReportDisplayTabelRelasi;
    
    private static final String cDefKeteranganFungsi = VARCHAR + AWAL_KURUNG + 150 + AKHIR_KURUNG ;
    @Column(name = "KeteranganFungsi", columnDefinition = cDefKeteranganFungsi)
    private String keteranganFungsi;
    
    private static final String cDefKdDepartemen = VARCHAR + AWAL_KURUNG + 255 + AKHIR_KURUNG ;
    @Column(name = "KdDepartemen", columnDefinition = cDefKdDepartemen)
    @NotNull(message = "settingdatafixed.kddepartemen.notnull")
    private String kdDepartemen;
    
    private static final String cDefStatusEnabled =INTEGER;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "settingdatafixed.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "settingdatafixed.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =INTEGER;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "settingdatafixed.version.notnull")
    private Integer version;

}
