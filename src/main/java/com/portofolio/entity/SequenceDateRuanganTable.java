package com.portofolio.entity;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.portofolio.entity.vo.SequenceDateRuanganTableId;
import com.portofolio.entity.vo.SequenceDateTableId;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Base class for all entities, but using String idString as 'id'
 * 
 * @author Syamsu
 */
@Entity
@Table(name = "SequenceDateRuanganTable_M")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SequenceDateRuanganTable {

	@EmbeddedId
	private SequenceDateRuanganTableId id;

	@Column(name = "IdTerakhir")
	@NotNull(message = "Id Terakhir Harus Diisi")
	private Integer idTerakhir;

	@Column(name = "NoRec", nullable = false)
	@NotNull(message = "NoRec Harus Diisi")
	private String NoRec;
	
	private static final String cDefVersion =INTEGER;
	@Version
	@Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
	@NotNull(message = "Version Harus Di Isi")
	private Integer version;
}
