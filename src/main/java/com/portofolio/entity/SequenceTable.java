package com.portofolio.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;

import com.portofolio.entity.vo.SequenceTableId;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Base class for all entities, but using String idString as 'id'
 * 
 * @author Adik
 */
@Entity
@Table(name = "SequenceTable_M")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SequenceTable implements Serializable {

	@EmbeddedId
	private SequenceTableId id;

	@Column(name = "NoRec", nullable = false)
	@NotNull(message = "NoRec Harus Diisi")
	private String NoRec;

	@Column(name = "idTerakhir")
	@NotNull(message = "Id Terakhir Harus Diisi")
	private Integer idTerakhir;

	private static final String cDefVersion =INTEGER;
	@Version
	@Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
	@NotNull(message = "Version Harus Di Isi")
	private Integer version;

}
