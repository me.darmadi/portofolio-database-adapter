package com.portofolio.dialect;

import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.StandardBasicTypes;

/**
 * @author Syamsu Rizal Ali
 */
public class SQLServerDialect extends org.hibernate.dialect.SQLServer2012Dialect {

	public SQLServerDialect() {
		super();
		
		registerFunction("concat_native", new StandardSQLFunction("concat", StandardBasicTypes.STRING));
		registerFunction("count_native", new SQLFunctionTemplate(StandardBasicTypes.LONG, "COUNT(?1)"));
		registerFunction("count_distinct", new SQLFunctionTemplate(StandardBasicTypes.LONG, "COUNT(DISTINCT(?1))"));
		
		registerFunction("ambil_tanggal", new SQLFunctionTemplate(StandardBasicTypes.INTEGER, "DAY(DATEADD(SECOND, ?1, '19700101 07:00:00:000'))"));
		registerFunction("ambil_bulan", new SQLFunctionTemplate(StandardBasicTypes.INTEGER, "MONTH(DATEADD(SECOND, ?1, '19700101 07:00:00:000'))"));
		registerFunction("ambil_tahun", new SQLFunctionTemplate(StandardBasicTypes.INTEGER, "YEAR(DATEADD(SECOND, ?1, '19700101 07:00:00:000'))"));
		
		registerFunction("di_tanggal_sama", new SQLFunctionTemplate(StandardBasicTypes.INTEGER, "DATEDIFF(day,DATEADD(SECOND, ?1, '19700101 07:00:00:000') , DATEADD(SECOND, ?2, '19700101 07:00:00:000'))"));
		registerFunction("di_bulan_sama", new SQLFunctionTemplate(StandardBasicTypes.INTEGER, "DATEDIFF(month,DATEADD(SECOND, ?1, '19700101 07:00:00:000') , DATEADD(SECOND, ?2, '19700101 07:00:00:000'))"));
		registerFunction("di_tahun_sama", new SQLFunctionTemplate(StandardBasicTypes.INTEGER, "DATEDIFF(year,DATEADD(SECOND, ?1, '19700101 07:00:00:000') , DATEADD(SECOND, ?2, '19700101 07:00:00:000'))"));
	}
}
